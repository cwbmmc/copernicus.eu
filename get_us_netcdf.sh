#!/bin/bash
# get_us_netcdf.sh
# By Stinger 20200612
# Updated 20200729

# Initialize
if [ ! -n $HOME ]; then HOME="/home/mmcww352" ; fi
if [ ! -n $PATH ]; then PATH=${HOME}/.local/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin ; fi
# Proxy for wget...
_proxy=`export | grep -c -i proxy`
if [ $_proxy -eq "0" ]; then
	export HTTPS_PROXY="http://proxy.cwb.gov.tw:8888/"
	export HTTP_PROXY="http://proxy.cwb.gov.tw:8888/"
	export NO_PROXY=".cwb.gov.tw,localhost,127.0.0.0/8,::1"
	export http_proxy="http://proxy.cwb.gov.tw:8888/"
	export https_proxy="http://proxy.cwb.gov.tw:8888/"
	export no_proxy=".cwb.gov.tw,localhost,127.0.0.0/8,::1"
fi

_ScrDir="${HOME}/scripts"		# Script location
_LocDir="${HOME}/netcdf"		# Satellite data location
# Location to store logs (links, currently)
_LogDir="${HOME}/logs"			# Log directory
_SecDir="${_ScrDir}/secrets"			# NOT USED, will remove after checked.
_S3AccFile="${_SecDir}/CopernicusOpenAccessHub"	# NOT USED, will remove after checked.
if [ ! -d ${_LogDir} ]; then mkdir -p ${_LogDir} ; fi
_LogFile="${_LogDir}/get_netcdf.log"	# General Log File
_LogFileBN="get_netcdf"			# Log File Base Name
_TmpDir="/dev/shm"			# Temporary directory
_S3HR=3					# How many hours of data to retrieve per hour, for Sentinel3 satellite
_Refresh=0				# To control force retrieve or not
_RunALsa=0
_Runj3=0
_RunS3=0

# Perhaps we don't need this anymore, see if any affection.
#if [ `date +%H` -eq "15" ]; then
# TODO: see if good or not
#	_Refresh=1
#	date +"%Y-%m-%d %H:%M" > $_LogFile
#	date +"%Y-%m-%d %H:%M" > ${_LogDir}/get_netcdf_S3.log
#	_S3HR=24
#else
#	_Refresh=0
#fi

# Satellite ALTIKA_saral
if [ "$1" == "ALTIKA" -o "$1" == "saral" -o "$1" == "ALTIKA_saral" ] || [ -z "$1" ]; then _RunALsa=1; else _RUNALsa=0; fi
# Satellite jason3
if [ "$1" == "jason3" ] || [ -z "$1" ]; then _Runj3=1; else _Runj3=0; fi
# Satellite Sentinel3
if [ "$1" == "sentinel3" -o "$1" == "Sentinel3" -o "$1" == "S3" -o "$1" == "s3" ] || [ -z "$1" ]; then _RunS3=1; else _RunS3=0; fi
# Satellite Sentinel3 large range, temporary not run in default case ( -z "$1" )
if [ "$1" == "S3L" -o "$1" == "s3l" ]; then _RunS3L=1; else _RunS3L=0; fi

# Clean
_Clean() {
	# Clean old files over ?(currently 5) days
	if [ "${_SatN}" == "sentinel-3-large" ]; then
		_MTime=0
	else
		_MTime=5
	fi
	find ${_LocDir}/${_SatN}/ -type f -mtime +${_MTime} -exec rm {} \; &
}

# Retrievement for ALTIKA_saral and jason3
_get_nasaNC() {
	# Retrieve the lastest directory
	_URLPrFxNm=c$(/usr/bin/curl ${_URL} | grep -io '<a href=['"'"'"][^"'"'"']*['"'"'"]' | sed -e 's/^<a href=["'"'"']//i' -e 's/["'"'"']$//i' -e 's/contents\.html//g' -e 's/^c//g' | awk 'BEGIN {max = 0} {if ($1+0 > max+0) max=$1} END {print max}' )
	if [ -n $_URLPrFxNm ]; then echo $_URLPrFxNm ; else echo "NOTHING inside ${_SatN}_URLPrFxNm" ; fi | tee -a $_LogFile 
	# Merge into a link
	_URLPrFx="${_URL}${_URLPrFxNm}"
	# Log the actual link to find the latest netcdf file
	echo $_URLPrFx >> $_LogFile
	# FileName to get
	_URLFN=`/usr/bin/curl ${_URLPrFx} | grep "\.nc\"" | grep name | sed -e 's/\"name\":\ \"//g' -e 's/\",//g' -e 's/ //g'`
	# Count the numbers of the files
	_URLNum=`echo $_URLFN | awk -F " " '{print NF}'`
	# For the use of getting the last 3 below
	let _URLNum=_URLNum-4
	# Create the directory if not existed
	if [ ! -d ${_LocDir}/${_SatN} ]; then mkdir -p ${_LocDir}/${_SatN} ; fi
	j=0

	# Sequential handling the file links
	for i in ${_URLFN}
	do
		if [ $j -eq "0" ]; then
			echo $j	 ${_URLPrFx}${i} > ${_LogDir}/${_SatN}.log
		else
			echo $j  ${_URLPrFx}${i} >> ${_LogDir}/${_SatN}.log
		fi
		# Get the last 3
		if [ "$j" -gt "${_URLNum}" ] || [ ${_Refresh} -eq "1" ] ; then
		# Use wget for resume
			/usr/bin/curl -L -C - -o ${_LocDir}/${_SatN}/${i} ${_URLPrFx}${i} &
			#/usr/bin/wget -c -O ${_LocDir}/${_SatN}/${i} ${_URLPrFx}${i} &
			# Log the links
			echo ${_URLPrFx}${i} >> ${_LogFile}
		fi
		let j=j+1
	done
	# Wait while all the files retrieved
	wait
}

# ALTIKA_saral & jason3 satellite retrievement
tail -1599 $_LogFile > ${_TmpDir}/${_LogFileBN}.log
mv ${_TmpDir}/${_LogFileBN}.log $_LogFile

if [ "${_RunALsa}" -eq "1" ]; then
	_URL="https://podaac-opendap.jpl.nasa.gov/opendap/allData/saral/preview/L2/XOGDR-SSHA/"
	_SatN="ALTIKA_saral"
	echo >> $_LogFile && echo >> $_LogFile
	echo "***************" >> $_LogFile
	date +"%Y-%m-%d %H:%M" >> $_LogFile
	echo "***************" >> $_LogFile
	_get_nasaNC
	_Clean
fi

if [ "${_Runj3}" -eq "1" ]; then
	_URL="https://podaac-opendap.jpl.nasa.gov/opendap/allData/jason3/preview/L2/GPS-OGDR/"
	_SatN="jason3"
	echo >> $_LogFile && echo >> $_LogFile
	echo "***************" >> $_LogFile
	date +"%Y-%m-%d %H:%M" >> $_LogFile
	echo "***************" >> $_LogFile
	_get_nasaNC
	_Clean
fi


# For Sentinel-3 satellite data retrievement
_URL="https://scihub.copernicus.eu/dhus"	# Default
#https://scihub.copernicus.eu/dhus/		#1
#https://coda.eumetsat.int/			#2
#https://data.sentinel.zamg.ac.at/		# Never works
#https://sentinels.space.noa.gr/dhus/		# Never works

if [ "${_RunS3}" -eq "1" -o "${_RunS3L}" -eq "1" ]; then
	if [ "${_RunS3}" -eq "1" ]; then _SatN="sentinel-3"; elif [ "${_RunS3L}" -eq "1" ]; then _SatN="sentinel-3-large"; fi

# Rolling the log file
	tail -2001 ${_LogDir}/${_LogFileBN}_${_SatN}.log > ${_TmpDir}/${_LogFileBN}_${_SatN}.log
	mv ${_TmpDir}/${_LogFileBN}_${_SatN}.log ${_LogDir}/${_LogFileBN}_${_SatN}.log
	echo >> ${_LogDir}/${_LogFileBN}_${_SatN}.log && echo >> ${_LogDir}/${_LogFileBN}_${_SatN}.log
	echo "***************" >> ${_LogDir}/${_LogFileBN}_${_SatN}.log
	date +"%Y-%m-%d %H:%M" >> ${_LogDir}/${_LogFileBN}_${_SatN}.log
	echo "***************" >> ${_LogDir}/${_LogFileBN}_${_SatN}.log

	export WGETRC=${_LocDir}/.wgetrc
	cd ${_LocDir}/${_SatN}
	if [ "${_RunS3}" -eq "1" ]; then 
		_URL="https://scihub.copernicus.eu/dhus"	# Default
		${_ScrDir}/dhusget.sh -u test -p test -t $_S3HR -F 'platformname:Sentinel-3 AND ( footprint:"Intersects(POLYGON((120.666831 26.646525,119.875815 26.272777,118.212529 24.516189,119.467095 21.533338,122.064308 21.637667,122.710774 25.295828,120.666831 26.646525)))")' -O ${_LocDir}/${_SatN}/ -o product -d $_URL >> ${_LogDir}/${_LogFileBN}_${_SatN}.log &
	elif [ "${_RunS3L}" -eq "1" ]; then
# Randomize the retrieve site, but seems only the default 1 working correctly
		#_RAND=$[ $RANDOM % 3 ];
		#echo -n "*** Random server this time is: " >> ${_LogDir}/${_LogFileBN}_${_SatN}.log
		#if [[ $_RAND =~ 0|1 ]]; then
		#	_URL="https://coda.eumetsat.int"
		#elif [ "$_RAND" -eq "1" ]; then
		#	_URL="https://data.sentinel.zamg.ac.at"
		#elif [ "$_RAND" -eq "2" ]; then
		#	_URL="https://sentinels.space.noa.gr/dhus"
		#else
		#	_URL="https://scihub.copernicus.eu/dhus"
		#fi
		#echo $_RAND ", $_URL ***" >> ${_LogDir}/${_LogFileBN}_${_SatN}.log

		if [ "`date +%M`" -lt "10" ]; then
			_URL="https://scihub.copernicus.eu/dhus"
		else
			_URL="https://coda.eumetsat.int"
		fi
		_S3HR=1
		${_ScrDir}/dhusget.sh -u test -p test -t $_S3HR -F 'platformname:Sentinel-3 AND ( footprint:"Intersects(POLYGON((99 1, 155 1,155 41,99 41, 99 1)))")' -O ${_LocDir}/${_SatN}/ -o product -d $_URL >> ${_LogDir}/${_LogFileBN}_${_SatN}.log &
	fi

	# The CLEAN
	wait
	if [ -d ${HOME}/dhusget_tmp ]; then rm -rf ${HOME}/dhusget_tmp; fi
	rm ${_LocDir}/${_SatN}/logs/log..log
	rm ${_LocDir}/${_SatN}/failed_MD5_check_list.txt
	if [! -d ${_LogDir}/${_SatN}-logs/ ]; then mkdir -p ${_LogDir}/${_SatN}-logs/ ; fi
	mv ${_LocDir}/${_SatN}/logs/* ${_LogDir}/${_SatN}-logs/
	rmdir ${_LocDir}/${_SatN}/logs
	mv ${_LocDir}/${_SatN}/OSquery-result.xml ${_LogDir}/${_SatN}-OSquery-result.xml
	mv ${_LocDir}/${_SatN}/product_list ${_LogDir}/${_SatN}-product_list
	mv ${_LocDir}/${_SatN}/products-list.csv ${_LogDir}/${_SatN}-products-list.csv
	mv ${_LocDir}/${_SatN}/offline_product_list.txt ${_LogDir}/${_SatN}-offline_product_list.txt
	_Clean
fi


