This is a very easy scripts to download satellite data from copernicus.eu site.
`dhusget.sh` and `odata-demo.sh` were get from https://cophub.copernicus.eu/dhus/ site, no modification required, just use it directly. (Remember to add execution permission)

`get_us_netcdf.sh` is the main script to call above scripts and to arrange related data.

You may want to change the variable of some locations:

- `_ScrDir` to where you stored these scripts
- `_LocDir` to where you want to store satellite data
- `_LogDir` to where you want to store the logs of execution

The generated satellite data directory name (under $_LocDir ) will be:

- `ALTIKA_saral` which is actually retrieved from https://podaac-opendap.jpl.nasa.gov/opendap/allData/saral/preview/L2/XOGDR-SSHA/
- `jason3` which is actually retrieved from https://podaac-opendap.jpl.nasa.gov/opendap/allData/jason3/preview/L2/GPS-OGDR/
- `sentinel-3` which is the data in the area around Taiwan
- `sentinel-3-large` which is the data in the north Pacific area.

Note that all the other directories such as al, cfo, h2b, j3, s3a, s3b are retrieved by another script:
https://gitlab.com/cwbmmc/nrt.cmems-du.eu

Call `get_us_netcdf.sh` will download ALTIKA, Jason3, Sentinel-3 satellite data by default, for Sentinel-3-Large, you have to call `get_us_netcdf.sh s3l`

Then the rest of the work is to made the cronjobs:

- `45 * * * *	${HOME}/scripts/get_us_netcdf.sh`
- `5,35 * * * *	${HOME}/scripts/get_us_netcdf.sh S3L`

and 

- `7 7 1 * *	/usr/bin/find ${HOME}/logs/* -type f -atime +30 -exec rm {} \;`
- `15 7 * * *	/usr/bin/find ${HOME}/netcdf/*/ -type f -ctime +3 -exec rm {} \;`

If you want to remove outdated data.
